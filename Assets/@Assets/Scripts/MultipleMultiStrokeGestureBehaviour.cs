using UnityEngine;
using System.Collections.Generic;
using GestureRecognizer;
using UnityEngine.UI;

public class MultipleMultiStrokeGestureBehaviour : MonoBehaviour {
	
	/// <summary>
	/// Disable or enable gesture recognition
	/// </summary>
	public bool isEnabled = true;
	
	/// <summary>
	/// Overwrite the XML file in persistent data path
	/// </summary>
	public bool forceCopy = false;
	
	/// <summary>
	/// Use the faster algorithm, however default (slower) algorithm has a better scoring system
	/// </summary>
	public bool useProtractor = false;
	
	/// <summary>
	/// The name of the gesture library to load. Do NOT include '.xml'
	/// </summary>
	public string libraryToLoad = "shapes";
	
	/// <summary>
	/// A new point will be placed if it is this further than the last point.
	/// </summary>
	public float distanceBetweenPoints = 10f;
	
	/// <summary>
	/// Minimum amount of points required to recognize a gesture.
	/// </summary>
	public int minimumPointsToRecognize = 10;
	
	/// <summary>
	/// Material for the line renderer.
	/// </summary>
	public Material lineMaterial;
	
	/// <summary>
	/// Start thickness of the gesture.
	/// </summary>
	public float startThickness = 0.25f;
	
	/// <summary>
	/// End thickness of the gesture.
	/// </summary>
	public float endThickness = 0.05f;
	
	/// <summary>
	/// Start color of the gesture.
	/// </summary>
	public Color startColor = new Color(0, 0.67f, 1f);
	
	/// <summary>
	/// End color of the gesture.
	/// </summary>
	public Color endColor = new Color(0.48f, 0.83f, 1f);
	
	/// <summary>
	/// Limits gesture drawing to a specific area
	/// </summary>
	public GestureLimitType gestureLimitType = GestureLimitType.None;
	
	/// <summary>
	/// RectTransform to limit gesture
	/// </summary>
	public RectTransform gestureLimitRectBounds;

	public int maxTouchId = 20;
	public Text message;
	
	/// <summary>
	/// Rect of the gestureLimitRectBounds
	/// </summary>
	Rect gestureLimitRect;
	
	/// <summary>
	/// Parent canvas of RectTransform to limit gesture.
	/// Set the pivot to bottom-left corner
	/// </summary>
	Canvas parentCanvas;
	
	/// <summary>
	/// Current platform.
	/// </summary>
	RuntimePlatform platform;
	
	/// <summary>
	/// Line renderer component. 
	/// </summary>
	Dictionary<int, LineRenderer> gestureRenderers;
	
	/// <summary>
	/// The position of the point on the screen. 
	/// </summary>
	Dictionary<int, Vector3> virtualKeyPositions;
	
	/// <summary>
	/// A new point. 
	/// </summary>
	Vector2 point;
	
	/// <summary>
	/// List of points that form the gesture. 
	/// </summary>
	Dictionary<int, List<MultiStrokePoint>> pointsArray;
	
	/// <summary>
	/// Vertex count of the line renderer. 
	/// </summary>
	Dictionary<int, int> vertexCounts;
	
	/// <summary>
	/// Loaded gesture library. 
	/// </summary>
	MultiStrokeLibrary ml;
	
	/// <summary>
	/// Recognized gesture. 
	/// </summary>
	Gesture gesture;
	
	/// <summary>
	/// Result. 
	/// </summary>
	Result result;
	
	/// <summary>
	/// This is the event to subscribe to.
	/// </summary>
	/// <param name="r">Result of the recognition</param>
	public delegate void GestureEvent(Result r);
	public static event GestureEvent OnRecognition;
	
	
	// Get the platform and apply attributes to line renderer.
	void Awake() {
		platform = Application.platform;

		InstantiateGestureRenderers (maxTouchId);
		virtualKeyPositions = new Dictionary<int, Vector3> ();
		vertexCounts = new Dictionary<int, int> ();
		pointsArray = new Dictionary<int, List<MultiStrokePoint>> ();

		for (int i = 0; i < maxTouchId; i++)
		{
			virtualKeyPositions.Add(i, Vector3.zero);
			vertexCounts.Add(i, 0);
			pointsArray.Add(i, new List<MultiStrokePoint>());
		}
	}

	void InstantiateGestureRenderers(int amount)
	{
		gestureRenderers = new Dictionary<int, LineRenderer> ();
		for (int i = 0; i < amount; i++)
		{
			GameObject gameObj = new GameObject("GestureRenderer" + i.ToString());
			gameObj.transform.SetParent(this.transform);
			gameObj.transform.localPosition = Vector3.zero;

			LineRenderer gestureRenderer = gameObj.AddComponent<LineRenderer>();
			gestureRenderer.SetVertexCount(0);
			gestureRenderer.material = lineMaterial;
			gestureRenderer.SetColors(startColor, endColor);
			gestureRenderer.SetWidth(startThickness, endThickness);

			gestureRenderers.Add(i, gestureRenderer);
		}
	}
	
	
	// Load the library.
	void Start() {
		ml = new MultiStrokeLibrary(libraryToLoad, forceCopy);
		
		if (gestureLimitType == GestureLimitType.RectBoundsClamp) {
			parentCanvas = gestureLimitRectBounds.GetComponentInParent<Canvas>();
			gestureLimitRect = RectTransformUtility.PixelAdjustRect(gestureLimitRectBounds, parentCanvas);
			gestureLimitRect.position += new Vector2(gestureLimitRectBounds.position.x, gestureLimitRectBounds.position.y);
		}
	}
	
	
	// Track user input and fire OnRecognition event when necessary.
	void Update() {
		
		// Track user input if GestureRecognition is enabled.
		if (isEnabled) {
			
			// If it is a touch device, get the touch position
			// if it is not, get the mouse position
			bool isTouching = false;
			//message.text = "";

			Touch[] touches = Input.touches;

			for (int i = 0; i < touches.Length; i++)
			{
				Touch touch = touches[i];
				//message.text += "Touch fingerID " + touch.fingerId.ToString() + " - " + touch.position.ToString() + "\n";

				if (touch.phase != TouchPhase.Ended) 
				{
					isTouching = true;

					if (!virtualKeyPositions.ContainsKey(touch.fingerId))
						virtualKeyPositions.Add(touch.fingerId, new Vector3(touch.position.x, touch.position.y)); 
					else
						virtualKeyPositions[touch.fingerId] = new Vector3(touch.position.x, touch.position.y);
				}
				else
				{
					isTouching = false;
				}

				if (isTouching)
				{
					switch (gestureLimitType) {
						
					case GestureLimitType.None:
						RegisterPoint(touch.fingerId);
						break;
						
					case GestureLimitType.RectBoundsIgnore:
						if (RectTransformUtility.RectangleContainsScreenPoint(gestureLimitRectBounds, virtualKeyPositions[i], null)) {
							RegisterPoint(touch.fingerId);
						}
						break;
						
					case GestureLimitType.RectBoundsClamp:

						if (!virtualKeyPositions.ContainsKey(touch.fingerId))
							virtualKeyPositions.Add(touch.fingerId, Utility.ClampPointToRect(virtualKeyPositions[i], gestureLimitRect)); 
						else
							virtualKeyPositions[touch.fingerId] = Utility.ClampPointToRect(virtualKeyPositions[i], gestureLimitRect);

						RegisterPoint(touch.fingerId);
						break;
					}
				}
				else
				{
					if (pointsArray[touch.fingerId].Count > minimumPointsToRecognize) 
					{
						MultiStroke multiStroke = new MultiStroke(pointsArray[touch.fingerId].ToArray());
						result = multiStroke.Recognize(ml);

						message.text = "FingerID: " + touch.fingerId + "Result: "  + result.Name;
						
						if (OnRecognition != null) {
							OnRecognition(result);
						}
					}
					else
						message.text = "HEHOH";
					ClearGesture(touch.fingerId);
				}
				
			}
		}
		
	}
	
	
	/// <summary>
	/// Register this point only if the point list is empty or current point
	/// is far enough than the last point. This ensures that the gesture looks
	/// good on the screen. Moreover, it is good to not overpopulate the screen
	/// with so much points.
	/// </summary>
	void RegisterPoint(int touchId) {
		point = new Vector2(virtualKeyPositions[touchId].x, -virtualKeyPositions[touchId].y);

		if (pointsArray[touchId].Count == 0 || (pointsArray[touchId].Count > 0 && Vector2.Distance(point, pointsArray[touchId][pointsArray[touchId].Count - 1].Point) > distanceBetweenPoints)) 
		{
			pointsArray[touchId].Add(new MultiStrokePoint(point.x, point.y, 0));
			vertexCounts[touchId]++;
			gestureRenderers[touchId].SetVertexCount(vertexCounts[touchId]);
			gestureRenderers[touchId].SetPosition(vertexCounts[touchId] - 1, Utility.WorldCoordinateForGesturePoint(virtualKeyPositions[touchId]));
		}
	}
	
	
	/// <summary>
	/// Remove the gesture from the screen.
	/// </summary>
	void ClearGesture(int touchId) {
		pointsArray[touchId].Clear();
		gestureRenderers[touchId].SetVertexCount(0);
		vertexCounts[touchId] = 0;
	}
}
