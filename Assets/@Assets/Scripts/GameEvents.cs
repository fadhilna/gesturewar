using UnityEngine;
using System.Collections;

public class GameEvents
{
	public delegate void OnQuestionChangedDelegate(int questionID);
	public delegate void OnInputEnteredDelegate(PlayerTag playerTag, int inputID);
	public delegate void SpecialClickedDelegate (PlayerTag playerTag);

	public static event OnQuestionChangedDelegate onQuestionChangedE;
	public static event OnInputEnteredDelegate onInputEnteredE;
	public static event SpecialClickedDelegate onSpecialClickedE;

	public static void OnQuestionChanged(int questionID) { if(onQuestionChangedE != null) onQuestionChangedE(questionID); }
	public static void OnInputEntered(PlayerTag playerTag, int inputID) { if (onInputEnteredE != null) onInputEnteredE(playerTag, inputID); }
	public static void OnSpecialClicked(PlayerTag playerTag) { if (onSpecialClickedE != null) onSpecialClickedE(playerTag); }
}