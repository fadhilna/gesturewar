public class GameParameters
{
	public static string[] questions = new string[]
	{
		"none",
		"triangle",
		"circle",
		"rectangle",
		"arrow"
	};

	public static int GetQuestionIdByName(string name)
	{
		for (int i = 0; i < questions.Length; i++)
		{
			if (questions[i] == name)
				return i;
		}

		return -1;
	}
}