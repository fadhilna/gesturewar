﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public static GameController Instance;

	public int initMaxGesture = 3;
	public int maxGesture = 3;
	public float maxTime = 15;
	public float time = 0;
	public bool isGame = false;
	public int sessionNum;
	public int maxSession = 4;
	public int stunDuration = 3;

	public int questionID  = 0;

	public Image timeBar;
	public GameObject startButton;

	public PlayerPanel bottomPlayer;
	public PlayerPanel topPlayer;

	void Awake()
	{
		Instance = this;
	}

	void Start()
	{
		//AudioController.PlayMusic ("Main Menu");
		maxGesture = initMaxGesture;
		bottomPlayer.gameObject.SetActive (false);
		topPlayer.gameObject.SetActive (false);
	}

	void OnEnable()
	{
		GameEvents.onSpecialClickedE += OnSpecialClicked;
	}

	void OnDisable()
	{
		GameEvents.onSpecialClickedE -= OnSpecialClicked;
	}

	void UpdateTime()
	{
		if (isGame)
		{
			time -= Time.deltaTime;
			Vector2 size = timeBar.GetComponent<RectTransform>().sizeDelta;
			size.x = 750 * time / maxTime;
			timeBar.GetComponent<RectTransform>().sizeDelta = size;

			if (time <= 0f) {
				isGame = false;
				EndSession();
			}
		}
	}
	
	void OnSpecialClicked (PlayerTag playerTag)
	{
		if (playerTag == PlayerTag.TOP)
			bottomPlayer.Stun (stunDuration);
		else
			topPlayer.Stun (stunDuration);
	}

	public void StartGame()
	{
		if (IsInSession())
			StartSession ();
		else {
			maxGesture = initMaxGesture;
			sessionNum = 0;
			bottomPlayer.Initialize();
			topPlayer.Initialize();
			StartSession();
		}
	}
	
	void StartSession()
	{
		sessionNum++;
		startButton.SetActive (false);
		
		time = maxTime;
		isGame = true;
		
		bottomPlayer.gameObject.SetActive (true);
		bottomPlayer.StartGame (maxGesture);
		
		topPlayer.gameObject.SetActive (true);
		topPlayer.StartGame (maxGesture);
	}

	void EndSession()
	{
		isGame = false;
		if (bottomPlayer.score > topPlayer.score) {
			bottomPlayer.SetAttackDefend(true);
			topPlayer.SetAttackDefend(false);
		}
		else if (bottomPlayer.score < topPlayer.score) {
			bottomPlayer.SetAttackDefend(false);
			topPlayer.SetAttackDefend(true);
		}

		if (!IsInSession()) 
		{
			EndGame();
		} 
		else 
		{
			//maxGesture++;
			startButton.SetActive (true);
			startButton.GetComponentInChildren<Text> ().text = "NEXT";
		}
	}

	bool IsInSession()
	{
		if (sessionNum < maxSession)
			return true;
		else
			return false;
	}

	void EndGame()
	{
		startButton.SetActive(true);
		startButton.GetComponentInChildren<Text>().text = "RESTART!";

		if (bottomPlayer.score > topPlayer.score) {
			bottomPlayer.SetWinLose(true);
			topPlayer.SetWinLose(false);
		}
		else if (bottomPlayer.score < topPlayer.score) {
			bottomPlayer.SetWinLose(false);
			topPlayer.SetWinLose(true);
		}
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Tab)) {
			StartSession();
		}

		UpdateTime ();
	}

	void UpdateQuestion()
	{
		questionID = GetNextQuestion ();
		GameEvents.OnQuestionChanged (questionID);
	}

	public int GetNextQuestion()
	{
		return 1 + Random.Range (0, maxGesture);
	}
}
