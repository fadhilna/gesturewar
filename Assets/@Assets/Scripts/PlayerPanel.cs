using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerPanel : MonoBehaviour
{
	public PlayerTag playerTag;
	public GameObject gamePanel;
	public GameObject winLosePanel;

	public Text gestureText;
	public Text scoreText;
	public Text winLoseText;
	public Image questionImage;

	public int currentQuestionID;
	public int score;
	public bool isSpecialUsed;

	public GameObject specialButton;
	public GestureBehaviour debugGestureBehaviour;
	public MultipleGestureBehaviour gestureBehaviour;

	void Awake()
	{
		debugGestureBehaviour = this.GetComponent<GestureBehaviour> ();
		gestureBehaviour = this.GetComponent<MultipleGestureBehaviour> ();
		SetActiveGestureBehaviour (false);
	}
	void OnEnable()
	{
		MultipleGestureBehaviour.OnRecognition += OnRecognition;
		GestureBehaviour.OnRecognition += OnRecognition;
	}

	void OnDisable()
	{
		MultipleGestureBehaviour.OnRecognition -= OnRecognition;
		GestureBehaviour.OnRecognition -= OnRecognition;
	}
	
	void OnRecognition (GestureRecognizer.Result r)
	{
		OnRecognition (r, PlayerTag.BOTTOM);
	}
	
	void OnRecognition (GestureRecognizer.Result r, PlayerTag playerTag)
	{
		if (playerTag == this.playerTag)
		{
			gestureText.text = r.Name + " for input: " + GameParameters.GetQuestionIdByName(r.Name);
			OnInputClicked(GameParameters.GetQuestionIdByName(r.Name));
		}
	}

	public void OnInputClicked(int questionID)
	{
		if (currentQuestionID == questionID) {
			score += GameController.Instance.sessionNum;
			SetScoreText();
		}

		GetNext ();
	}

	void SetScoreText()
	{
		scoreText.text = score.ToString();
	}

	void GetNext()
	{
		currentQuestionID = GameController.Instance.GetNextQuestion ();
		questionImage.sprite = Resources.Load<Sprite> (currentQuestionID.ToString ());;
	}

	public void Initialize()
	{
		specialButton.SetActive (true);
		score = 0;
	}

	public void StartGame(int maxGesture)
	{
		SetActiveGestureBehaviour (true);
		winLosePanel.SetActive (false);
		gamePanel.SetActive (true);

		SetScoreText ();
		GetNext ();
	}

	public void SetAttackDefend(bool isAttack)
	{
		if (isAttack)
			ShowResultPanel ("Sim Salabim!");
		else
			ShowResultPanel ("OUCH!");

		
		SetActiveGestureBehaviour (false);
	}

	public void SetWinLose(bool isWin)
	{
		if (isWin)
			ShowResultPanel("You Win!\nScore: " + score.ToString ());
		else
			ShowResultPanel("You Lose!\nScore: " + score.ToString ());

		SetActiveGestureBehaviour (false);
	}

	public void ShowResultPanel(string resultText)
	{
		gamePanel.SetActive (false);
		winLosePanel.SetActive (true);

		winLoseText.text = resultText;
	}

	void SetActiveGestureBehaviour(bool isActive)
	{
		gestureBehaviour.isEnabled = isActive;

		if (debugGestureBehaviour != null)
			debugGestureBehaviour.isEnabled = isActive;
	}

	public void OnSpecial()
	{
		isSpecialUsed = true;
		specialButton.SetActive (false);
		GameEvents.OnSpecialClicked (this.playerTag);
	}

	public void Stun(float duration)
	{
		StartCoroutine (StunThis (duration));
	}

	IEnumerator StunThis(float duration)
	{
		SetActiveGestureBehaviour (false);
		specialButton.SetActive (false);
		yield return new WaitForSeconds(duration);
		SetActiveGestureBehaviour (true);
		if (!isSpecialUsed)
			specialButton.SetActive (true);
	}
}

public enum PlayerTag
{
	BOTTOM,
	TOP
}